# Order Manager 1.0

Manage orders on a board, these are created through a Rest API

## Quick start

1. Restore all plug-ins from nuget
2. Compile the solution with Visual Studio 2019 or higher
3. We strongly recommend using the new Microsoft Edge web browser to debug

## Rest API Test

A Postman collection is attached to perform the tests on the services

## How It Works

The project uses In Memory Cache as a storage system, an Api Rest was developed to manipulate the product catalog and generate orders.

A dashboard web interface was developed to view the orders and change their status using Jquery as a javascript framework and web socket to update the orders in real time.

## Credits

This project was developed by Irvin de la Rosa.

## License

Order Manager 1.0 is an open source project licensed under MIT.