@Imports OrderManager.Areas.HelpPage.ModelDescriptions
@ModelType ComplexTypeModelDescription
@Html.DisplayFor(Function(m) Model.Properties, "Parameters")
