﻿using OrderManager.Core.Caching;
using OrderManager.Core.Domain.Common;
using OrderManager.Core.Domain.Orders;
using OrderManager.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Services.Catalog
{
    public partial class OrderService
    {
        #region Fields
        protected readonly IStaticCacheManager _cacheManager;
        #endregion

        public OrderService(IStaticCacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        private List<Product> GetProductList()
        {
            var listResult = new List<Product>();
         
            var cacheKey = new CacheKey("allProducts");

            if (_cacheManager.IsSet(cacheKey))
            {
                listResult = _cacheManager.Get(cacheKey, () => new List<Product>());            
            }

            return listResult;
        }

        private List<Order> GetOrderList()
        {
            var listResult = new List<Order>();
   
            var cacheKey = new CacheKey("allOrders");

            if (_cacheManager.IsSet(cacheKey))
            {
                listResult = _cacheManager.Get(cacheKey, () => new List<Order>());             
            }

            return listResult;
        }


        private void SaveList(List<Order> orders)
        {
            var cacheKey = new CacheKey("allOrders");

            if (!_cacheManager.IsSet(cacheKey))
                cacheKey.CacheTime = CachingDefaults.CacheTime;

            _cacheManager.Set(cacheKey, orders);
        }

        public Response<int> Insert(Order order)
        {
            Response<int> response = new Response<int>()
            {
                Result = Response.Reply.success,
                Message = "La orden se ha generado con éxito."
            };

            var productList = GetProductList();
            
            var productsInventoring = new[] { new { Id = 0, Stock = 0 } }.ToList();
            productsInventoring.Clear();

            foreach (var product in order.Products)
            {
                var productEntity = productList.FirstOrDefault(exp => exp.Id == product.Id);

                if (productEntity == null)
                {
                    response.Result = Response.Reply.notSuccess;
                    response.Message = $"No se encontró el producto { product.Name }, favor de verificar";

                    return response;
                }

                if (productEntity.StockQuantity == 0 || product.Quantity > productEntity.StockQuantity)
                {
                    response.Result = Response.Reply.notSuccess;
                    response.Message = $"El producto {productEntity.Name} no tiene inventario disponible";

                    return response;
                }

                productsInventoring.Add(new { product.Id, Stock = productEntity.StockQuantity  - product.Quantity});
            }

            foreach (var inventoring in productsInventoring)
            {
                var productEntity = productList.FirstOrDefault(exp => exp.Id == inventoring.Id);
                productEntity.StockQuantity = inventoring.Stock;
            }

            var orderList = GetOrderList();
            
            order.Id = orderList.Any() ? orderList.Max(m => m.Id) + 1 : 1;
            order.StatusType = OrderStatusType.PENDING;
            order.NextStatus = GetNextStatus(OrderStatusType.PENDING);
            response.Data = order.Id;
            orderList.Add(order);
            SaveList(orderList);

            return response;
        }

        public Response Delete(int id)
        {
            Response response = new Response()
            {
                Result = Response.Reply.success,
                Message = "La orden se ha eliminado con éxito."
            };

            var orderList = GetOrderList();

            orderList.RemoveAll(exp => exp.Id == id);

            return response;
        }

        public Response<Order> GetById(int id)
        {
            Response<Order> response = new Response<Order>()
            {
                Result = Response.Reply.success
            };

            var orderEntity = GetOrderList().FirstOrDefault(exp => exp.Id == id);

            if (orderEntity == null)
            {
                response.Result = Response.Reply.error;
                response.Message = "No se encontró la orden.";

                return response;
            }

            response.Data = orderEntity;
            return response;

        }

        public Response<List<Order>> GetAll()
        {
            Response<List<Order>> response = new Response<List<Order>>
            {
                Result = Response.Reply.success,
                Data = GetOrderList()
            };

            return response;
        }

        public Response<Order> ChangeStatus(int id)
        {
            Response<Order> response = new Response<Order>()
            {
                Result = Response.Reply.success
            };

            var orderEntity = GetOrderList().FirstOrDefault(exp => exp.Id == id);

            if(orderEntity == null)
            {
                response.Result = Response.Reply.error;
                response.Message = "No se encontró la orden.";

                return response;
            }

            orderEntity.StatusType = GetNextStatus(orderEntity.StatusType);
            orderEntity.NextStatus = GetNextStatus(orderEntity.StatusType);

            response.Data = orderEntity;

            return response;
        }

        public Response<Order> Cancel(int id)
        {
            Response<Order> response = new Response<Order>()
            {
                Result = Response.Reply.success
            };

            var orderEntity = GetOrderList().FirstOrDefault(exp => exp.Id == id);

            if (orderEntity == null)
            {
                response.Result = Response.Reply.error;
                response.Message = "No se encontró la orden.";

                return response;
            }

            orderEntity.StatusType = OrderStatusType.CANCELED;

            response.Data = orderEntity;

            return response;
        }



        private string GetNextStatus(string currentStatus)
        {
            string nextStatusResult = currentStatus;

            switch (currentStatus)
            {
                case OrderStatusType.PENDING:
                    nextStatusResult = OrderStatusType.INPROCESS;
                    break;

                case OrderStatusType.INPROCESS:
                    nextStatusResult = OrderStatusType.COMPLETED;
                    break;

                case OrderStatusType.COMPLETED:
                    nextStatusResult = OrderStatusType.DELIVERED;
                    break;   
            }

            return nextStatusResult;
        }

    }
}
