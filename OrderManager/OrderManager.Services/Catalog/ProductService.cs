﻿using OrderManager.Core;
using OrderManager.Core.Caching;
using OrderManager.Core.Domain.Common;
using OrderManager.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Services.Catalog
{
    public partial class ProductService
    {
        #region Fields
        protected readonly IStaticCacheManager _cacheManager;
        #endregion

        public ProductService(IStaticCacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        private List<Product> GetList()
        {
            var listResult = new List<Product>();
    
            var cacheKey = new CacheKey("allProducts");

            if (_cacheManager.IsSet(cacheKey))
            {
                listResult = _cacheManager.Get(cacheKey, () => new List<Product>());               
            }

            return listResult;
        }

        private void SaveList(List<Product> products)
        {
            var cacheKey = new CacheKey("allProducts");

            if (!_cacheManager.IsSet(cacheKey))
                cacheKey.CacheTime = CachingDefaults.CacheTime;

            _cacheManager.Set(cacheKey, products);
        }

        public Response Insert(Product product)
        {
            Response response = new Response()
            {
                Result = Response.Reply.success,
                Message = "El producto se ha agregado con éxito."
            };

            var productList = GetList();

            if (productList.Any(an => an.SKU == product.SKU))
            {
                response.Result = Response.Reply.notSuccess;
                response.Message = "El SKU ya se encuentra agregado, favor de verificar";

                return response;
            }

            product.Id = productList.Any() ? productList.Max(m => m.Id) + 1 : 1;
            productList.Add(product);
            SaveList(productList);

            return response;
        }

        public Response Delete(int id)
        {
            Response response = new Response()
            {
                Result = Response.Reply.success,
                Message = "El producto se ha eliminado con éxito."
            };

            var productList = GetList();

            if (productList.RemoveAll(exp => exp.Id == id) == 0)
            {
                return new Response()
                {
                    Result = Response.Reply.notSuccess,
                    Message = "No se encontró el producto, favor de verificar."
                };
            }

            //SaveList(productList);

            return response;
        }

        public Response Update(Product product)
        {
            Response response = new Response()
            {
                Result = Response.Reply.success,
                Message = "El producto se ha actualizado con éxito."
            };

            var productList = GetList();

            var productEntity = productList.FirstOrDefault(exp => exp.Id == product.Id);

            if (productEntity == null)
            {
                return new Response()
                {
                    Result = Response.Reply.notSuccess,
                    Message = "No se encontró el producto, favor de verificar."
                };
            }

            if (productList.Any(exp => exp.SKU == product.SKU && exp.Id != product.Id))
            {
                return new Response()
                {
                    Result = Response.Reply.notSuccess,
                    Message = "Ya hay otro producto con el mismo SKU, favor de verificar"
                };
            }


            productEntity.SKU = product.SKU;
            productEntity.Name = product.Name;
            productEntity.Price = product.Price;
            productEntity.StockQuantity = product.StockQuantity;
            productEntity.Description = product.Description;

            return response;
        }

        public Response<Product> GetById(int id)
        {
            Response<Product> response = new Response<Product>()
            {
                Result = Response.Reply.success
            };

            var productEntity = GetList().FirstOrDefault(exp => exp.Id == id);

            if (productEntity == null)
            {
                response.Result = Response.Reply.error;
                response.Message = "No se encontró el producto.";

                return response;
            }


            response.Data = productEntity;
            return response;

        }

        public Response<List<Product>> GetAll()
        {
            Response<List<Product>> response = new Response<List<Product>>
            {
                Result = Response.Reply.success,
                Data = GetList()
            };

            return response;
        }
    }
}
