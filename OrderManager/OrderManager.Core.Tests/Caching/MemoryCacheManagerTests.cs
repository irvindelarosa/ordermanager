﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Caching.Memory;
using OrderManager.Core.Caching;

namespace OrderManager.Core.Tests.Caching
{
    [TestClass]
    public class MemoryCacheManagerTests
    {
        private MemoryCacheManager _staticCacheManager;

        [TestInitialize]
        public void Setup()
        {
            _staticCacheManager = new MemoryCacheManager(new MemoryCache(new MemoryCacheOptions()));
        }

        [TestMethod]
        public void Can_set_and_get_object_from_cache()
        {
            _staticCacheManager.Set(new CacheKey("some_key_1"), 3);
            Assert.AreEqual(_staticCacheManager.Get(new CacheKey("some_key_1"), () => 0), 3);
        }

        [TestMethod]
        public void Can_validate_whetherobject_is_cached()
        {
            _staticCacheManager.Set(new CacheKey("some_key_1"), 3);
            _staticCacheManager.Set(new CacheKey("some_key_2"), 4);

            Assert.IsTrue(_staticCacheManager.IsSet(new CacheKey("some_key_1")));
            Assert.IsFalse(_staticCacheManager.IsSet(new CacheKey("some_key_3")));
        }

        [TestMethod]
        public void Can_clear_cache()
        {
            _staticCacheManager.Set(new CacheKey("some_key_1"), 3);

            _staticCacheManager.Clear();

            Assert.IsFalse(_staticCacheManager.IsSet(new CacheKey("some_key_1")));
        }

        [TestMethod]
        public void Can_perform_lock()
        {
            var key = new CacheKey("Nop.Task");
            var expiration = TimeSpan.FromMinutes(2);

            var actionCount = 0;
            var action = new Action(() =>
            {
                Assert.IsTrue(_staticCacheManager.IsSet(key));

                Assert.IsFalse(_staticCacheManager.PerformActionWithLock(key.Key, expiration,
                    () => Assert.Fail("Action in progress")));

                if (++actionCount % 2 == 0)
                    throw new ApplicationException("Alternating actions fail");
            });

            Assert.IsTrue(_staticCacheManager.PerformActionWithLock(key.Key, expiration, action));
            Assert.AreEqual(actionCount, 1);

            _staticCacheManager.Invoking(a => a.PerformActionWithLock(key.Key, expiration, action)).Should().Throw<ApplicationException>();
            Assert.AreEqual(actionCount, 2);

            Assert.IsTrue(_staticCacheManager.PerformActionWithLock(key.Key, expiration, action));
            Assert.AreEqual(actionCount, 3);
        }
    }
}
