﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using OrderManager.Core.Domain.Orders;
using OrderManager.Services.Catalog;
using OrderManager.Web.Infrastructure;

namespace OrderManager.Web.Hubs
{
    public class OrderHub : Hub
    {                     

        public static void OrderAddNotification(int id)
        {
            var orderService = new OrderService(ApiBaseController.MemoryCacheManagerInstance);

            var orderData = orderService.GetById(id);

            if (orderData.Result == Core.Domain.Common.Response.Reply.success)
                GlobalHost.ConnectionManager.GetHubContext<OrderHub>().Clients.All.addOrden(orderData.Data);
        }

        public IEnumerable<Order> GetAll()
        {
            var orderService = new OrderService(ApiBaseController.MemoryCacheManagerInstance);

            var orderList = orderService.GetAll();

            return orderList.Data;
        }

        public void ChangeStatus(int id)
        {
            var orderService = new OrderService(ApiBaseController.MemoryCacheManagerInstance);

            var orderData = orderService.ChangeStatus(id);

            if (orderData.Result == Core.Domain.Common.Response.Reply.success)
                Clients.All.updateOrden(orderData.Data);
        }

        public void Cancel(int id)
        {
            var orderService = new OrderService(ApiBaseController.MemoryCacheManagerInstance);

            var orderData = orderService.Cancel(id);

            if (orderData.Result == Core.Domain.Common.Response.Reply.success)
                Clients.All.updateOrden(orderData.Data);
        }

       
    }
}