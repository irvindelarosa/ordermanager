﻿using OrderManager.Core.Domain.Common;
using OrderManager.Core.Domain.Orders;
using OrderManager.Services.Catalog;
using OrderManager.Web.Hubs;
using OrderManager.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrderManager.Web.Controllers
{
    public class OrderController : ApiBaseController
    {
        private readonly OrderService orderService;

        public OrderController()
        {
            orderService = new OrderService(MemoryCacheManagerInstance);
        }
          
        public Response Post(Order order)
        {
            Response<dynamic> resultResponse = new Response<dynamic>();

            var result = orderService.Insert(order);

            resultResponse.Result = result.Result;
            resultResponse.Message = result.Message;

            if(resultResponse.Result == Response.Reply.success)
            {
                OrderHub.OrderAddNotification(result.Data);
            }

            return resultResponse;
        }
      
    }
}
