﻿using OrderManager.Core.Domain.Common;
using OrderManager.Core.Domain.Products;
using OrderManager.Services.Catalog;
using OrderManager.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrderManager.Web.Controllers
{
    public class ProductController : ApiBaseController
    {
        private readonly ProductService productService;

        public ProductController()
        {
            productService = new ProductService(MemoryCacheManagerInstance);
        }

        public Response Get()
        {
            Response<dynamic> resultResponse = new Response<dynamic>();

            var result = productService.GetAll();

            resultResponse.Result = result.Result;
            resultResponse.Message = result.Message;

            if (resultResponse.Result == Response.Reply.success)
            {
                resultResponse.Data = result.Data;
            }

            return resultResponse;
        }
      
        public Response Get(int id)
        {
            Response<dynamic> resultResponse = new Response<dynamic>();

            var result = productService.GetById(id);

            resultResponse.Result = result.Result;
            resultResponse.Message = result.Message;

            if (resultResponse.Result == Response.Reply.success)
            {
                resultResponse.Data = result.Data;
            }

            return resultResponse;
        }

        public Response Post(Product model)
        {
            Response<dynamic> resultResponse = new Response<dynamic>();

            var result = productService.Insert(model);

            resultResponse.Result = result.Result;
            resultResponse.Message = result.Message;
            
            return resultResponse;
        }

       
        public Response Put(Product model)
        {
            Response<dynamic> resultResponse = new Response<dynamic>();

            var result = productService.Update(model);

            resultResponse.Result = result.Result;
            resultResponse.Message = result.Message;

            return resultResponse;
        }
    
        public Response Delete(int id)
        {
            Response<dynamic> resultResponse = new Response<dynamic>();

            var result = productService.Delete(id);

            resultResponse.Result = result.Result;
            resultResponse.Message = result.Message;

            return resultResponse;
        }
    }
}
