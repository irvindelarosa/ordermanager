﻿using Microsoft.Extensions.Caching.Memory;
using OrderManager.Core.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrderManager.Web.Infrastructure
{
    public class ApiBaseController : ApiController
    {
		private static readonly IStaticCacheManager _memoryCacheManager = new MemoryCacheManager(new MemoryCache(new MemoryCacheOptions()));

		public static IStaticCacheManager MemoryCacheManagerInstance => _memoryCacheManager;
	}
}
