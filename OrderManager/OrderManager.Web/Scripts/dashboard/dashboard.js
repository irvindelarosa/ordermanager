﻿$(function myfunction() {

    /** Fields **/
    var orderHubProxy = $.connection.orderHub;


    /** SignalR Methods **/    
    orderHubProxy.client.addOrden = function (order) {
        console.log(order);
        $('#pending .row:first').append(getObjectTemplate(order));
    };

    orderHubProxy.client.updateOrden = function (order) {
        console.log(order);
        $(`#order-${order.Id}`).remove();
        $(`#${order.StatusType.replace(/ /g, '').toLowerCase()} .row:first`).append(getObjectTemplate(order));
    }; 

    $.connection.hub.start()
        .done(function () {
            console.log('Now connected, connection ID=' + $.connection.hub.id);

            orderHubProxy.server.getAll().done(function (orders) {
                $.each(orders, function () {
                    var order = this;
                    $(`#${order.StatusType.replace(/ /g, '').toLowerCase()} .row:first`).append(getObjectTemplate(order));                                      
                });
            }).fail(function (error) {
                console.log('Error: ' + error);
            });

        })
        .fail(function () { console.log('Could not Connect!'); });


    /** Events **/
    $('#myTabContent').on('click', 'button[data-change]', function () {
        orderHubProxy.server.changeStatus($(this).attr('data-change')).done(function () {
            console.log('Change status invocation success');
        }).fail(function (error) {
            console.log('Error: ' + error);
        });
    });

    $('#myTabContent').on('click', 'button[data-cancel]', function () {
        orderHubProxy.server.cancel($(this).attr('data-cancel')).done(function () {
            console.log('Cancel order invocation success');
        }).fail(function (error) {
            console.log('Error: ' + error);
        });
    });
   
    /** Custom Methods **/       
    function getObjectTemplate(order) {

        let buttonTemplate = `
            <button type="button" data-change="${order.Id}" class="btn btn-default btn-sm" style="width: 100px;">
                <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> ${order.NextStatus}
            </button>
            <button type="button" data-cancel="${order.Id}" class="btn btn-default btn-sm" style="right: 15px;width: 100px;bottom: 0px;position: absolute;">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Cancel
            </button>`;

        let productTemplate = [];

        for (var i = 0; i < order.Products.length; i++) {
            let product = order.Products[i];
            productTemplate.push(`<tr><td>${product.Name}</td><td>${product.Quantity}</td></tr>`);                       
        }

        let orderTemplate = `
              <div class="col-sm-6 col-md-4" id="order-${order.Id}">
                    <div class="thumbnail">
                        <div class="caption">
                            <span>Order ${order.OrderNumber}</span><span class="label label-default" style="float:right;">${order.StatusType}</span>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <span>PRODUCTS</span>
                                    <hr style="margin-top: 1px;">
                                    <table style="width: 100%;">
                                        <tbody>${productTemplate.join('')}</tbody>
                                    </table>
                                </div>
                                <div class="col-xs-6 col-md-5" style="text-align: right;min-height: 100px;">
                                    ${(order.StatusType !== 'Delivered' && order.StatusType !== 'Canceled' ? buttonTemplate : '' )}                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;


        return orderTemplate;
    }

});