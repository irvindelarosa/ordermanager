﻿using System;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OrderManager.Core.Caching;
using OrderManager.Core.Domain.Common;
using OrderManager.Core.Domain.Products;
using OrderManager.Services.Catalog;

namespace OrderManager.Web.Tests.Service
{
    [TestClass]
    public class ProductServiceTest
    {
        private ProductService _productService;
        private MemoryCacheManager _staticCacheManager;

        [TestInitialize]
        public void Setup()
        {
            _staticCacheManager = new MemoryCacheManager(new MemoryCache(new MemoryCacheOptions()));
            _productService = new ProductService(_staticCacheManager);
        }

        [TestMethod]
        public void Can_add()
        {
            var product = new Product()
            {
                Name = "Test 1",
                SKU = "12345",
                Description = "Este es un producto test",
                Price = 13.95,
                StockQuantity = 5
            };

            var expected = Response.Reply.success;
            var result = _productService.Insert(product);
            Assert.AreEqual(expected, result.Result);
        }

        [TestMethod]
        public void Can_not_add_duplicate_sku()
        {

            var product = new Product()
            {
                Name = "Test 2",
                SKU = "654321",
                Description = "Este es un producto test",
                Price = 13.95,
                StockQuantity = 5
            };

            var expected = Response.Reply.notSuccess;
            _productService.Insert(product);
            var result = _productService.Insert(product);
            Assert.AreEqual(expected, result.Result);
        }

        [TestMethod]
        public void Can_get_all()
        {
            var expected = Response.Reply.success;
            var result = _productService.GetAll();
            Assert.AreEqual(expected, result.Result);
        }

        [TestMethod]
        public void Can_get_by_id()
        {
            var product = new Product()
            {
                Name = "Test 3",
                SKU = "46512",
                Description = "Este es un producto test",
                Price = 13.95,
                StockQuantity = 5
            };

            _productService.Insert(product);

            var expected = Response.Reply.success;
            var result = _productService.GetById(1);
            Assert.AreEqual(expected, result.Result);
        }

        [TestMethod]
        public void Can_delete()
        {
            var product = new Product()
            {
                Name = "Test 4",
                SKU = "97648",
                Description = "Este es un producto test",
                Price = 13.95,
                StockQuantity = 5
            };

            _productService.Insert(product);

            var expected = Response.Reply.success;
            var result = _productService.Delete(1);
            Assert.AreEqual(expected, result.Result);

            Can_get_all();
        }

        [TestMethod]
        public void Can_update()
        {
            var product = new Product()
            {
                Name = "Test 5",
                SKU = "00001",
                Description = "Este es un producto test",
                Price = 13.95,
                StockQuantity = 5
            };

            _productService.Insert(product);
                   
            product.Name = "Test 6";
            product.SKU = "8897845";
            product.Description = "Hola";
            product.Price = 31.58;
            product.StockQuantity = 8;

            var expected = Response.Reply.success;
            var result = _productService.Update(product);
            Assert.AreEqual(expected, result.Result);
        }

        [TestMethod]
        public void Can_not_update_with_duplicate_sku()
        {
            var product = new Product()
            {
                Name = "Test 7",
                SKU = "90001",
                Description = "Este es un producto test 7",
                Price = 13.95,
                StockQuantity = 5
            };

            _productService.Insert(product);

            var product2 = new Product()
            {
                Name = "Test 8",
                SKU = "90002",
                Description = "Este es un producto test 8",
                Price = 8.95,
                StockQuantity = 5
            };

            _productService.Insert(product2);

            var product3 = new Product()
            {
                Id = 2,
                Name = "Test 9",
                SKU = "90001",
                Description = "Este es un producto test 9",
                Price = 10.95,
                StockQuantity = 5
            };
          
            var expected = Response.Reply.notSuccess;
            var result = _productService.Update(product3);
            Assert.AreEqual(expected, result.Result);
        }


    }
}
