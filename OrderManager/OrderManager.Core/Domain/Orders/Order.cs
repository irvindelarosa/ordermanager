﻿using OrderManager.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderManager.Core.Domain.Orders
{
    public partial class Order : BaseEntity
    {
        public string StatusType { get; set; }

        public string NextStatus { get; set; }

        public string OrderNumber => $"#{Id.ToString().PadLeft(6, '0')}";
        
        public List<OrderProduct> Products { get; set; }

    }
}
