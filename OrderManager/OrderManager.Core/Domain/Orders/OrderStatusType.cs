﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderManager.Core.Domain.Orders
{
    /// <summary>
    ///  Represent the status type for all orders
    /// </summary>
    public static partial class OrderStatusType
    {
        public const string PENDING = "Pending";

        public const string INPROCESS = "In Process";

        public const string COMPLETED = "Completed";

        public const string DELIVERED = "Delivered";

        public const string CANCELED = "Canceled";
    }
}
