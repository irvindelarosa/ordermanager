﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderManager.Core.Domain.Products
{
    /// <summary>
    /// Represents an product
    /// </summary>
    public partial class Product : BaseEntity
    {
        /// <summary>
        /// get or set the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// get or set a description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// get or set the sku value
        /// </summary>
        public string SKU { get; set; }

        /// <summary>
        /// get or ser the price
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// get or set de stock quantity for inventoring
        /// </summary>
        public int StockQuantity { get; set; }

        //TODO: Add more properties if requiered like category, cost, discount, etc
    }
}
