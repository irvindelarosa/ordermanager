﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderManager.Core.Domain.Common
{
    public partial class Response
    {
        public enum Reply { success, notSuccess, error }
        public Reply Result { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }

    public class Response<T> : Response
    {
        public T Data { get; set; }
    }
}
